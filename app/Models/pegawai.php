<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
//use Illuminate\Database\Eloquent\Model;

class pegawai extends Illuminate\Database\Eloquent\Model
{
    protected $table = 'pegawai';

    protected $fillable = [
    	'nomor_induk_pegawai',
    	'nama',
    	'jabatan',
    	'alamat'
    ];
}
