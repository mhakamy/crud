<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Post;

class Post extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'title',
        'body'
    ];

    public function create()
    {
        return view('post.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|unique:posts|max:150',
            'body' => 'required', 
        ]);

        $input = $request->all();

        $post = Post::create($input);
        
        return back()->with('success',' Post baru berhasil dibuat.');
    }


}
